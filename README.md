# Game Of Life

Game of Life implementations in different programming languages. 

Implement Game of Life [cellular automaton](https://en.wikipedia.org/wiki/Cellular_automaton) in a programming language of your choice given the description below:

### Cellular Automata Rules
 
 - Any live cell with fewer than two live neighbours dies, as if by underpopulation.
 - Any live cell with two or three live neighbours lives on to the next generation.
 - Any live cell with more than three live neighbours dies, as if by overpopulation.
 - Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
 
 ### Condensed Rules
 
 - Any live cell with two or three live neighbours survives.
 - Any dead cell with three live neighbours becomes a live cell.
 - All other live cells die in the next generation. Similarly, all other dead cells stay dead.


## References
- [Wikipedia](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life)
- [Blog post by Greg Wilson](https://www.software-carpentry.org/blog/2010/06/the-cowichan-problems.html)
